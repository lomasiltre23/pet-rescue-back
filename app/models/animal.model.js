const mongoose = require('mongoose');

const AnimalSchema = mongoose.Schema({
    name: String,
    species: String,
    breed: String,
    color: String,
    age: Number,
    location: String,
    gender: String
},{
    timestamps: true
});

module.exports = mongoose.model('Animal', AnimalSchema);