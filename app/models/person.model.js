const mongoose = require('mongoose');

const PersonSchema = mongoose.Schema({
    name: String,
    email: String,
    adress: String,
    phone: Number,
    age: Number,
    desc: String
},{
    timestamps: true
});

module.exports = mongoose.model('Person', PersonSchema);