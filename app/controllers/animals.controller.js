const Animal = require('../models/animal.model.js');

// Create and Save a new animal
exports.create = (req, res) => {
    // Validate data    
    if(!req.body){
        return res.status(400).send({
            message: "Animal data is empty!!"
        });
    }

    // Create new animal
    const animal = new Animal({
        name: req.body.name || "No name",
        species: req.body.species,
        breed: req.body.breed,
        color: req.body.color,
        age: req.body.age,
        location: req.body.location,
        publication: req.body.publication
    });

    // Add new animal to database

    animal.save().then( data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred!!"
        });
    });
};

// Retrieve and return all animals from the database.
exports.findAll = (req, res) => {
    Animal.find().then(animals => {
        res.send(animals);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred!!"
        })
    });
};

// Update an animal identified by the id in the request
exports.update = (req, res) => {

};

// Delete an animal with the specified id in the request
exports.delete = (req, res) => {
    Animal.findByIdAndRemove(req.params.id)
    .then(animal => {
        if(!animal){
            return res.status(404).send({
                message: "Animal not found with id " + req.params.id
            });
        }
        res.send({message: "Animal deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Animal not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Could not delete animal with id " + req.params.id
        });
    })
};