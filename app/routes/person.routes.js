module.exports = (app) => {
    const person = require('../controllers/person.controller.js');

    // Create new person
    app.post("/person", person.create);
    
    // Get all persons
    app.post("/person", person.findAll);
}