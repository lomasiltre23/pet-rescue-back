module.exports = (app) => {
    const animals = require('../controllers/animals.controller.js');

    // Create New Animals
    app.post("/animals", animals.create);

    // Get all animals
    app.get("/animals", animals.findAll);

    // Update an animal with Id
    app.put('/animals/:id', animals.update);

    // Delete an animal with Id
    app.delete('/animals/:id', animals.delete);
}