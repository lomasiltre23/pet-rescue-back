const express = require('express');
const bodyParser = require('body-parser');

// Create express app
const app = express();
// CORS permissions
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers','Content-Type');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
// Parse requests of Content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// Parse requests of content-type - application/json
app.use(bodyParser.json());
// Configuring database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Database connection success!");
}).catch(err => {
    console.log("Could not connect to database: ", err);
    process.exit();
})
// Define a simple route
app.get("/", (req, res) => {
    res.json({"message": "Welcome"});
});

/****************/
/*    Routes    */
/****************/
require('./app/routes/animal.routes.js')(app);
require('./app/routes/person.routes.js')(app);
/****************/

app.listen(3000, () => {
    console.log("Server on port: 3000");
})